
module sistema (
	clk_clk,
	reset_reset_n,
	regs_export);	

	input		clk_clk;
	input		reset_reset_n;
	output	[7:0]	regs_export;
endmodule
